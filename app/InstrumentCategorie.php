<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstrumentCategorie extends Model
{
    protected $fillable=[
    	'category','instrument'];
    protected $hidden =[
    	'created_at','update_at'];
}
