<?php

namespace App;

trait Helpers
{
    public static function hRespData($code, $data)
    {
        $r = config('rcode.' . $code);
        $r['result'] = [
            'data' => $data,
        ];

    }

    public static function hRespError($code, $data)
    {
        $r = config('rcode.' . $code);
        $r['result'] = [
            'errors' => $data,
        ];

    }

    public static function hResp($code, $data, $error = false)
    {

        $r = config('rcode.' . $code);
        $r['result'] = [
            ($error ? 'errors' : 'data') => $data,
        ];
        return response()->json($r, $code);

    }

}
