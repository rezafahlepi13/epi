<?php

/**
 * This file is part of Windmel.
 * http://athronsoft.co.id/windmel - https://bitbucket.org/athronsoft/windmel
 * Copyright (C) 2018 Gilang Albathin Nurhabibi [Athron98] - athron.poster@gmail
 * Copyright (C) 2008-2018 Athronsoft IT Solution
 *
 * Windmel is not free software: you can't redistribute it and/or modify.
 *
 * @author :
 *        - 2018 athron98
 * @changelog :
 *        - 22/02/2018 2:02:29 AM Just Created [athron98]
 *        - 26/02/2018 8:07:40 PM Added hidden field [athron98]
 *
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $fillable = [
        'segment', 'msg',
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];
}
