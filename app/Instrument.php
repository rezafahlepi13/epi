<?php

/**
 * This file is part of Windmel.
 * http://athronsoft.co.id/windmel - https://bitbucket.org/athronsoft/windmel
 * Copyright (C) 2018 Gilang Albathin Nurhabibi [Athron98] - athron.poster@gmail
 * Copyright (C) 2008-2018 Athronsoft IT Solution
 *
 * Windmel is not free software: you can't redistribute it and/or modify.
 *
 * @author :
 *        - 2018 athron98
 * @changelog :
 *        - 22/02/2018 2:02:29 AM Just Created [athron98]
 *        - 26/02/2018 8:07:40 PM Added hidden field [athron98]
 *
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instrument extends Model
{

    protected $fillable = [
        'vendor', 'instrument',
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function vendor()
    {
        return $this->belongsTo('\App\Vendor', 'vendor');
    }

    public function category()
    {
        return $this->hasMany('App\InstrumentCategory', 'instrument', 'id');
    }

    public function log()
    {
        return $this->hasMany('App\InstrumentLog', 'instrument_log', 'id');
    }

    public function profile()
    {
        return $this->hasMany('App\InstrumentProfile', 'instrument_profile', 'id');
    }

    public function status()
    {
        return $this->hasMany('App\InstrumentStatus', 'instrument_status', 'id');
    }

    protected function validate(array $data)
    {
        return Validator::make($data, [
            'vendor' => 'required|numeric|exists:vendors,id',
            'instrument' => 'required|string|max:255|unique:instruments',
        ]);
    }
}
