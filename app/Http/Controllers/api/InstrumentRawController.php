<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InstrumentRawController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $d = \App\InstrumentRaw::all();
        $r = config('rcode.200');
        $r['result'] = [
            'data' => $d,
        ];

        return response()->json($r, $r['code']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return \App\InstrumentRaw::create($data); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $r = config('rcode.406');
        $r['result'] = [
            'msg' => 'Please use acp for create, update and deleting',
        ];
        return response()->json($r, $r['code']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($id == 'Docs') {
            return view('apidocs.InstrumentRaw');
        }

        $d = \App\InstrumentRaw::find($id);

        if ($d != null) {

            $r = config('rcode.200');
            $r['result'] = [
                'data' => $d,
            ];

        } else {
            $r = config('rcode.204');
        }

        return response()->json($r, $r['code']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $r = config('rcode.406');
        $r['result'] = [
            'msg' => 'Please use acp for create, update and deleting',
        ];
        return response()->json($r, $r['code']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $r = config('rcode.406');
        $r['result'] = [
            'msg' => 'Please use acp for create, update and deleting',
        ];
        return response()->json($r, $r['code']);
    }
}
