<?php

/**
 * This file is part of Windmel.
 * http://athronsoft.co.id/windmel - https://bitbucket.org/athronsoft/windmel
 * Copyright (C) 2018 Gilang Albathin Nurhabibi [Athron98] - athron.poster@gmail
 * Copyright (C) 2008-2018 Athronsoft IT Solution
 *
 * Windmel is not free software: you can't redistribute it and/or modify.
 *
 * @author :
 *        - 2018 athron98
 * @changelog :
 *        - 22/02/2018 2:02:29 AM Just Created [athron98]
 *
 */

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $r = config('rcode.200');

        if (isset($_GET['category'])) {
            $d = \App\InstrumentCategory::where('category', $_GET['category'])->get();
            foreach ($d as $k => $v) {
                $d[$k]->vendor_id = \App\instrument::find($d[$k]->instrument)->vendor()->get()->first()['id'];
                $d[$k]->vendor = \App\instrument::find($d[$k]->instrument)->vendor()->get()->first()['vendor'];
            }

            $d = $d->groupBy('vendor');
            $dd = [];

            foreach ($d as $k => $v) {
                array_push($dd, ['id' => $d[$k]->first()['vendor_id'], 'vendor' => $k]);
            }

            $d = $dd;
            if (sizeof($dd) < 1) {
                $r = config('rcode.204');
                $d = null;
            }
        } else {
            $d = \App\Vendor::all();

            if ($d->count() < 1) {
                $r = config('rcode.204');
                $d = null;
            }
        }

        $r['result'] = [
            'data' => $d,
        ];

        return response()->json($r, $r['code']);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(array $data)
    {
        return \App\Vendor::create($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $r = config('rcode.406');
        $r['result'] = [
            'msg' => 'Please use acp for create, update and deleting',
        ];
        return response()->json($r, $r['code']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        if ($id == 'Docs') {
            return view('apidocs.vendor');
        }

        $d = \App\Vendor::find($id);

        if ($d != null) {

            $r = config('rcode.200');
            $r['result'] = [
                'data' => $d,
            ];

        } else {
            $r = config('rcode.204');
        }

        return response()->json($r, $r['code']);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $r = config('rcode.406');
        $r['result'] = [
            'msg' => 'Please use acp for create, update and deleting',
        ];
        return response()->json($r, $r['code']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $r = config('rcode.406');
        $r['result'] = [
            'msg' => 'Please use acp for create, update and deleting',
        ];
        return response()->json($r, $r['code']);
    }

}
