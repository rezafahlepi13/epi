<?php

/**
 * This file is part of Windmel.
 * http://athronsoft.co.id/windmel - https://bitbucket.org/athronsoft/windmel
 * Copyright (C) 2018 Gilang Albathin Nurhabibi [Athron98] - athron.poster@gmail
 * Copyright (C) 2008-2018 Athronsoft IT Solution
 *
 * Windmel is not free software: you can't redistribute it and/or modify.
 *
 * @author :
 *        - 2018 athron98
 * @changelog :
 *        - 22/02/2018 2:02:29 AM Just Created [athron98]
 *
 */

namespace App\Http\Controllers\api\acp;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SegmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $d = \App\Segment::all();

        return $this->hResp($d ? 200 : 204, $d, !$d);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(array $data)
    {
        return \App\Segment::create($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $v = \App\Segment::validate($request->all());
        $f = $v->fails();

        return $this->hResp($f ? 406 : 201, $f ? $v->errors() : $this->create($request->all()), $f);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $d = \App\Segment::find($id);

        return $this->hResp($d ? 200 : 204, $d, !$d);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $d = \App\Segment::find($id);
        $d->vendor = $request->vendor;
        return $d->update();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $v = \App\Segment::validate($request->all());
        $f = $v->fails();

        return $this->hResp($f ? 304 : 205, $f ? $v->errors() : $this->edit($id, $request), $f);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $d = \App\Segment::destroy($id);

        return $this->hResp($d ? 200 : 204, $d, !$d);
    }

}
