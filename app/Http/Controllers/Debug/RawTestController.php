<?php

namespace App\Http\Controllers\Debug;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RawTestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, $vendor, $model, $id)
    {

        $fs = config('isetting.' . $vendor . '.' . $model . '.Sparator.Field');
        $rs = config('isetting.' . $vendor . '.' . $model . '.Sparator.Record');
        $fc = config('isetting.' . $vendor . '.' . $model . '.Parameter.DataFieldCount');
        $hc = config('isetting.' . $vendor . '.' . $model . '.Parameter.HeadFieldCount');
        $fd = config('isetting.' . $vendor . '.' . $model . '.Definition.Count.' . $fc . '.Fields');

        $path = storage_path('samples/' . $vendor . '/' . $model . '/sample-' . $id);
        if (!file_exists($path)) {
            return null;
        }
            
        $d = file_get_contents($path);
        $d = explode($rs, str_replace(chr(13),"", $d) );
            
        $r = [];
        foreach ($d as $k => $raw) {
            if (strpos($raw, chr(23)) !== false) {
                $raw = explode(chr(23), $raw);
                $rr = "";
                foreach ($raw as $rk => $rv) {
                    $rr .= substr($rv, 4);
                }
                $raw = $rr;
            } 

            $raw = str_replace(chr(13), "", $raw);
            $raw = explode(chr(2), $raw);
            $raw = $raw[sizeof($raw) - 1];
            $d[$k] = explode('|', $raw);
            $fieldCount = sizeof($d[$k]);
            $t = [
                'count' => $fieldCount,
                'segments' => substr_count($raw, $fs),
                'data' => [],
                'raw' => $raw,
            ];
            switch ($fieldCount) {
                case $fc:
                    if ($d[$k][2] != "") {
                    }
                    $tt = [];
                    foreach ($fd as $key => $value) {
                        if ($value != null) {
                            $tt[$value] = trim($d[$k][$key]);
                        }
                    }
                    array_push($t['data'], $tt);

                    break;
                case $hc:
                    break;

                default:

                    break;
            }
            if ($fieldCount == $fc || $fieldCount == $hc) {
                if ($request->only('parameter')) {
                    if (strpos(substr(trim($d[$k][2]), 4), $request->only('parameter')['parameter']) !== false) {
                        array_push($r, $t);
                    }
                } else {

                    array_push($r, $t);
                }

            }
        }

        return $r;
    }
}
