<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('/Instrument', 'api\\InstrumentController');
Route::resource('/Sample', 'api\\SampleController');
Route::resource('/Raw', 'api\\RawController');
Route::resource('/acp/Category', 'api\\acp\\CategoryController');
Route::resource('/acp/Instrument', 'api\\acp\\InstrumentController');
Route::resource('/acp/Segment', 'api\\acp\\SegmentController');
Route::resource('/acp/Vendor', 'api\\acp\\VendorController');

Route::resource('/Category', 'api\\CategoryController');
Route::resource('/Instrument', 'api\\InstrumentController');
Route::resource('/Segment', 'api\\SegmentController');
Route::resource('/Vendor', 'api\\VendorController');
