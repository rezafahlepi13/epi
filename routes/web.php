<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
	return view('welcome');
});

Route::get('/acp', function () {
	return view('admin');
});

Route::get('/Docs/{id}', function ($id) {
	return view('docs');
})->where('id', '(.*)');

Route::get('/directives/{id}', function ($id) {
	return view('directives.' . $id);
})->where('id', '(.*)');

Route::get('/Debug/RawTest/{vendor}/{model}/{id}', 'Debug\\RawTestController');
