( function ( module ) {
    'use strict';
    require( 'angular' )
        .module( 'admin.run', [] )
    ;
    module.exports = 'admin.run';
} )( module );

