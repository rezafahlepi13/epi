( function ( module ) {
    'use strict';
    module.exports = function ( $stateProvider, $urlRouterProvider, $httpProvider, $locationProvider ) {

        $locationProvider.hashPrefix('');

        $httpProvider.defaults.xsrfCookieName = 'csrftoken';
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

        var states = [ {
            name: 'dashboard',
            url: '/',
            templateUrl: e64url( '/directives/acp/page/dashboard' ),
        }, {
            name: 'auth',
            abstract: true,
            templateUrl: e64url('/directives/acp/page/auth')
        }, {
            url: '/signin',
            name: 'auth.signin',
            templateUrl: e64url('/directives/acp/page/auth/signin')
        }, {
            url: '/signup',
            name: 'auth.signup',
            templateUrl: e64url('/directives/acp/page/auth/signup')
        }, {
            url: '/auth/forgot-password',
            name: 'auth.forgot-password',
            templateUrl: e64url('/directives/acp/page/auth/forgot-password')
        }, {
            url: '/auth/signout',
            name: 'auth.signout',
            templateUrl: e64url('/directives/acp/page/auth/signout')

        } ];
        states.forEach( function ( state ) {
            $stateProvider.state( state.name, state );
        } );
        $urlRouterProvider.otherwise( '/' );
    };
    module.exports.$inject = [ '$stateProvider', '$urlRouterProvider', '$httpProvider', '$locationProvider' ];

} )( module );

