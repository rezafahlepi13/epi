( function ( module ) {
    'use strict';
    require( 'angular' )
        .module( 'admin.config', [] )
        .config( require( './route' ) )
	;
    module.exports = 'admin.config';
} )( module );

