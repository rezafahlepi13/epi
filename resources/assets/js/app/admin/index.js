( function () {
    'use strict';
    window.jQuery = window.$ = require( 'jquery' );
    require( './plugins' );
    window.moment = require('moment/moment');
    window.btoa = require( 'btoa' );
    window.e64enable = false;
    window.e64url = function ( str ) {
        if ( window.e64enable ) {
            return '/-/' + window.btoa( str );
        } else {
            return str;
        }
    };
    require( 'angular' )
        .module( 'admin', [
            require( './core' ),
            require( './configs' ),
            require( './directives' ),
            require( './controllers' ),
            require( './runs' )
        ] )
        
    ;
} )();

