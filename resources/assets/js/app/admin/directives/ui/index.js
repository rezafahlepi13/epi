( function ( module ) {
    'use strict';
    require( 'angular' )
        .module( 'admin.directives.ui', [] )
        .directive('uiNavbar',require('./navbar'))
    ;
    module.exports = 'admin.directives.ui';
} )( module );

