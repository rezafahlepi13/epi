( function ( module ) {
    'use strict';
    //module.exports.$inject = [ '$http' ];
    module.exports = function ( $http, $mdMenu, $state, $rootScope ) {
        // Runs during compile
        return {
            replace: false,
            transclude: true,
            restrict: 'AE', // E = Element, A = Attribute, C = Class, M = Comment
            templateUrl: e64url( '/directives/acp/ui/navbar' ),
            link: function ( $scope, iElm, iAttrs, controller ) {
                var self = $scope;
                self.view = function () {
                    switch ( $rootScope.state ) {
                    case 'auth':
                    case 'auth.forgot-password':
                    case 'auth.signin':
                    case 'auth.signup':
                    case 'landing':
                        return 0;
                    default:
                        return 1;
                    }
                };
                self.isLanding = function () {
                    if ( $rootScope.state == 'landing' ) {
                        return 1;
                    }
                    return 0;
                };
                self.openMenu = function ( $mdMenu, ev ) {
                    $mdMenu.open( ev );
                };
                self.closeAlert = function ( index ) {
                    $rootScope.alerts.splice( index, 1 );
                };
            }
        };

    };

    module.exports.$inject = [ '$http', '$mdMenu', '$state', '$rootScope' ];
} )( module );

