( function ( module ) {
    'use strict';
    require( 'angular' )
        .module( 'admin.directives', [
            require( './ui' ),
        ] )
    ;
    module.exports = 'admin.directives';
} )( module );

