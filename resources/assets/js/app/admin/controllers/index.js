( function ( module ) {
    'use strict';
    require( 'angular' )
        .module( 'admin.controllers', [] )
        .controller( 'MainController', require( './Main' ) );
    module.exports = 'admin.controllers';
} )( module );

