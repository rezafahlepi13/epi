( function ( module ) {
    'use strict';
    require( '@uirouter/angularjs/release/angular-ui-router' );
    require( '@uirouter/angularjs/release/stateEvents' );
    require( 'velocity-animate/velocity' );
    require( 'velocity-animate/velocity.ui' );
    require( 'angular-velocity' );
    require( 'ng-img-crop/compile/unminified/ng-img-crop' );
    require( 'angular-base64' );
//    require( 'angular-xeditable/dist/js/xeditable' );

        require( 'angular' )
        .module( 'docs.more', [
            'ui.router',
            'ui.router.state.events',
            'angular-velocity',
            'ngImgCrop',
            'base64',
//            'xeditable',
        ] );
    module.exports = 'docs.more';
} )( module );

