( function ( module ) {
    'use strict';
    require( 'angular' )
        .module( 'docs.core', [
            require( 'angular-sanitize' ),
            require( 'angular-animate' ),
            require( 'angular-aria' ),
            require( 'angular-messages' ),
            require( 'angular-material' ),
            require( 'angular-loading-bar' ),
            require( 'angular-ui-bootstrap' ),
            require( 'angular-translate' ),
            require( 'angular-translate-loader-static-files' ),
            require( 'angular-storage' ),
            require( 'ng-file-upload' ),
            require( './more' ),
        ] );
    module.exports = 'docs.core';
} )( module );

