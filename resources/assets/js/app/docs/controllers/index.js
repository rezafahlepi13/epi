( function ( module ) {
    'use strict';
    require( 'angular' )
        .module( 'docs.controllers', [] )
        .controller( 'MainController', require( './Main' ) );
    module.exports = 'docs.controllers';
} )( module );

