<!DOCTYPE html>
<html ng-app="admin">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

	<title>Windmel - Admin Control Panel</title>

	<link rel="stylesheet" href="{{  mix('css/web.css') }}">

    <script>
        window.web = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
	<script src="{{ mix('js/web.js') }}"></script>

</head>
    <body ui-navbar>
        <ui-view class="velocity-opposites-transition-slideUpIn" data-velocity-opts="{ stagger: 650 }" data-velocity-opts="{ duration: 1350 }" /> </body>

</html>
