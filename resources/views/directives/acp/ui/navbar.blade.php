<div class="app">
    <!-- aside -->
    <aside id="aside" class="app-aside modal fade " role="menu">
        <div class="left">
            <div class="box bg-white">
                <div class="navbar md-whiteframe-z1 no-radius blue">
                    <!-- brand -->
                    <a class="navbar-brand">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve" style="
                            width: 24px; height: 24px;">
                        <path d="M 50 0 L 100 14 L 92 80 Z" fill="rgba(139, 195, 74, 0.5)"></path>
                    <path d="M 92 80 L 50 0 L 50 100 Z" fill="rgba(139, 195, 74, 0.8)"></path>
                <path d="M 8 80 L 50 0 L 50 100 Z" fill="#f3f3f3"></path>
            <path d="M 50 0 L 8 80 L 0 14 Z" fill="rgba(220, 220, 220, 0.6)"></path>
        </svg>
        <img src="../images/logo.png" alt="." style="max-height: 36px; display:none">
        <span class="hidden-folded m-l inline">Windmel</span>
    </a>
    <!-- / brand -->
</div>
<div class="box-row">
    <div class="box-cell scrollable hover">
        <div class="box-inner">
            <div class="p hidden-folded blue-50" style="background-image:url(../images/bg.png); background-size:cover">
                <div class="rounded w-64 bg-white inline pos-rlt">
                    <img src="../images/a0.jpg" class="img-responsive rounded">
                </div>
                <a class="block m-t-sm" ui-toggle-class="hide, show" target="#nav, #account">
                    <span class="block font-bold">John Smith</span>
                    <span class="pull-right auto">
                        <i class="fa inline fa-caret-down"></i>
                        <i class="fa none fa-caret-up"></i>
                    </span>
                    john.smith@gmail.com
                </a>
            </div>
            <div id="nav">
                <nav ui-nav>
                    <ul class="nav">
                        <li>
                            <a md-ink-ripple>
                                <span class="pull-right text-muted">
                                    <i class="fa fa-caret-down"></i>
                                </span>
                                <i class="pull-right up"><b class="badge no-bg">2</b></i>
                                <i class="icon mdi-action-settings-input-svideo i-20"></i>
                                <span class="font-normal">Dashboard</span>
                            </a>
                            <ul class="nav nav-sub">
                                <li>
                                    <a md-ink-ripple href="index.html">Dashboard</a>
                                </li>
                                <li>
                                    <a md-ink-ripple href="dashboard.analysis.html">Analysis</a>
                                </li>
                                <li>
                                    <a md-ink-ripple href="dashboard.wall.html">Wall</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
            <div id="account" class="hide m-v-xs">
                <nav>
                    <ul class="nav">
                        <li>
                            <a md-ink-ripple href="page.profile.html">
                                <i class="icon mdi-action-perm-contact-cal i-20"></i>
                                <span>My Profile</span>
                            </a>
                        </li>
                        <li>
                            <a md-ink-ripple href="page.settings.html">
                                <i class="icon mdi-action-settings i-20"></i>
                                <span>Settings</span>
                            </a>
                        </li>
                        <li>
                            <a md-ink-ripple href="lockme.html">
                                <i class="icon mdi-action-exit-to-app i-20"></i>
                                <span>Logout</span>
                            </a>
                        </li>
                        <li class="m-v-sm b-b b"></li>
                        <li>
                            <div class="nav-item" ui-toggle-class="folded" target="#aside">
                                <label class="md-check">
                                    <input type="checkbox">
                                    <i class="purple no-icon"></i>
                                    <span class="hidden-folded">Folded aside</span>
                                </label>
                            </div>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
<nav>
    <ul class="nav b-t b">
        <li>
            <a href="" target="_blank" md-ink-ripple>
                <i class="icon mdi-action-help i-20"></i>
                <span>Help &amp; Feedback</span>
            </a>
        </li>
    </ul>
</nav>
</div>
</div>
</aside>
<!-- / aside -->
<!-- content -->
<div id="content" class="app-content" role="main">
<div class="box">
<!-- Content Navbar -->
<div class="navbar md-whiteframe-z1 no-radius blue">
<!-- Open side - Naviation on mobile -->
<a md-ink-ripple  data-toggle="modal" data-target="#aside" class="navbar-item pull-left visible-xs visible-sm"><i class="mdi-navigation-menu i-24"></i></a>
<!-- / -->
<!-- Page title - Bind to $state's title -->
<div class="navbar-item pull-left h4">Dashboard</div>
<!-- / -->
<!-- Common tools -->
<ul class="nav nav-sm navbar-tool pull-right">
    <li>
        <a md-ink-ripple ui-toggle-class="show" target="#search">
            <i class="mdi-action-search i-24"></i>
        </a>
    </li>
    <li>
        <a md-ink-ripple data-toggle="modal" data-target="#user">
            <i class="mdi-social-person-outline i-24"></i>
        </a>
    </li>
    <li class="dropdown">
        <a md-ink-ripple data-toggle="dropdown">
            <i class="mdi-navigation-more-vert i-24"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-scale pull-right pull-up text-color">
            <li><a href>Single-column view</a></li>
            <li><a href>Sort by date</a></li>
            <li><a href>Sort by name</a></li>
            <li class="divider"></li>
            <li><a href>Help &amp; feedback</a></li>
        </ul>
    </li>
</ul>
<div class="pull-right" ui-view="navbar@"></div>
<!-- / -->
<!-- Search form -->
<div id="search" class="pos-abt w-full h-full blue hide">
    <div class="box">
        <div class="box-col w-56 text-center">
            <!-- hide search form -->
            <a md-ink-ripple class="navbar-item inline"  ui-toggle-class="show" target="#search"><i class="mdi-navigation-arrow-back i-24"></i></a>
        </div>
        <div class="box-col v-m">
            <!-- bind to app.search.content -->
            <input class="form-control input-lg no-bg no-border" placeholder="Search" ng-model="app.search.content">
        </div>
        <div class="box-col w-56 text-center">
            <a md-ink-ripple class="navbar-item inline"><i class="mdi-av-mic i-24"></i></a>
        </div>
    </div>
</div>
<!-- / -->
</div>
<!-- Content -->
<div class="box-row">
<div class="box-cell">
    <div class="box-inner padding" ui-view>

    </div>
</div>
</div>
</div>
</div>
<!-- / content -->
</div>
