<!DOCTYPE html>
<html ng-app="web">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Documentation</title>
	<link rel="stylesheet" href="{{  mix('css/web.css') }}">
</head>
<body ng-controller="MainController">
	<div ng-include="'//127.0.0.1/directives/docs/page/vendor'" >
	</div>
	<script src="{{ mix('js/web.js') }}"></script>
</body>
</html>
