<?php

/**
 * This file is part of Windmel.
 * http://athronsoft.co.id/windmel - https://bitbucket.org/athronsoft/windmel
 * Copyright (C) 2018 Gilang Albathin Nurhabibi [Athron98] - athron.poster@gmail
 * Copyright (C) 2008-2018 Athronsoft IT Solution
 *
 * Windmel is not free software: you can't redistribute it and/or modify.
 *
 * @author :
 *        - 2018 athron98
 * @changelog :
 *        - 22/02/2018 2:02:29 AM Just Created [athron98]
 *
 */

return [
    100 => [
        'code' => 100,
        'msg' => 'Continue',
    ],
    101 => [
        'code' => 101,
        'msg' => 'Switching Protocols',
    ],
    102 => [
        'code' => 102,
        'msg' => 'Processing',
    ],
    103 => [
        'code' => 103,
        'msg' => 'Early Hints',
    ],
    200 => [
        'code' => 200,
        'msg' => 'OK',
    ],
    201 => [
        'code' => 201,
        'msg' => 'Created',
    ],
    202 => [
        'code' => 202,
        'msg' => 'Accepted',
    ],
    203 => [
        'code' => 203,
        'msg' => 'Non-Authoritative Information',
    ],
    204 => [
        'code' => 204,
        'msg' => 'No Content',
    ],
    205 => [
        'code' => 205,
        'msg' => 'Reset Content',
    ],
    206 => [
        'code' => 206,
        'msg' => 'Partial Content',
    ],
    207 => [
        'code' => 207,
        'msg' => 'Multi-Status',
    ],
    208 => [
        'code' => 208,
        'msg' => 'Already Reported',
    ],
    226 => [
        'code' => 226,
        'msg' => 'IM Used',
    ],
    300 => [
        'code' => 300,
        'msg' => 'Multiple Choices',
    ],
    301 => [
        'code' => 301,
        'msg' => 'Moved Permanently',
    ],
    302 => [
        'code' => 302,
        'msg' => 'Found',
    ],
    303 => [
        'code' => 303,
        'msg' => 'See Other',
    ],
    304 => [
        'code' => 304,
        'msg' => 'Not Modified',
    ],
    305 => [
        'code' => 305,
        'msg' => 'Use Proxy',
    ],
    306 => [
        'code' => 306,
        'msg' => 'Switch Proxy',
    ],
    307 => [
        'code' => 307,
        'msg' => 'Temporary Redirect',
    ],
    308 => [
        'code' => 308,
        'msg' => 'Permanent Redirect',
    ],
    400 => [
        'code' => 400,
        'msg' => 'Bad Request',
    ],
    401 => [
        'code' => 401,
        'msg' => 'Unauthorized',
    ],
    402 => [
        'code' => 402,
        'msg' => 'Payment Required',
    ],
    403 => [
        'code' => 403,
        'msg' => 'Forbidden',
    ],
    404 => [
        'code' => 404,
        'msg' => 'Not Found',
    ],
    405 => [
        'code' => 405,
        'msg' => 'Method Not Allowed',
    ],
    406 => [
        'code' => 406,
        'msg' => 'Not Acceptable',
    ],
    407 => [
        'code' => 407,
        'msg' => 'Proxy Authentication Required',
    ],
    408 => [
        'code' => 408,
        'msg' => 'Request Timeout',
    ],
    409 => [
        'code' => 409,
        'msg' => 'Conflict',
    ],
    410 => [
        'code' => 410,
        'msg' => 'Gone',
    ],
    411 => [
        'code' => 411,
        'msg' => 'Length Required',
    ],
    412 => [
        'code' => 412,
        'msg' => 'Precondition Failed',
    ],
    413 => [
        'code' => 413,
        'msg' => 'Payload Too Large',
    ],
    414 => [
        'code' => 414,
        'msg' => 'URI Too Long',
    ],
    415 => [
        'code' => 415,
        'msg' => 'Unsupported Media Type',
    ],
    416 => [
        'code' => 416,
        'msg' => 'Range Not Satisfiable',
    ],
    417 => [
        'code' => 417,
        'msg' => 'Expectation Failed',
    ],
    418 => [
        'code' => 418,
        'msg' => 'I\'m a teapot',
    ],
    421 => [
        'code' => 421,
        'msg' => 'Misdirected Request',
    ],
    422 => [
        'code' => 422,
        'msg' => 'Unprocessable Entity',
    ],
    423 => [
        'code' => 423,
        'msg' => 'Locked',
    ],
    424 => [
        'code' => 424,
        'msg' => 'Failed Dependency',
    ],
    426 => [
        'code' => 426,
        'msg' => 'Upgrade Required',
    ],
    428 => [
        'code' => 428,
        'msg' => 'Precondition Required',
    ],
    429 => [
        'code' => 429,
        'msg' => 'Too Many Requests',
    ],
    431 => [
        'code' => 431,
        'msg' => 'Request Header Fields Too Large',
    ],
    451 => [
        'code' => 451,
        'msg' => 'Unavailable For Legal Reasons',
    ],
    500 => [
        'code' => 500,
        'msg' => 'Internal Server Error',
    ],
    501 => [
        'code' => 501,
        'msg' => 'Not Implemented',
    ],
    502 => [
        'code' => 502,
        'msg' => 'Bad Gateway',
    ],
    503 => [
        'code' => 503,
        'msg' => 'Service Unavailable',
    ],
    504 => [
        'code' => 504,
        'msg' => 'Gateway Timeout',
    ],
    505 => [
        'code' => 505,
        'msg' => 'HTTP Version Not Supported',
    ],
    506 => [
        'code' => 506,
        'msg' => 'Variant Also Negotiates',
    ],
    507 => [
        'code' => 507,
        'msg' => 'Insufficient Storage',
    ],
    508 => [
        'code' => 508,
        'msg' => 'Loop Detected',
    ],
    510 => [
        'code' => 510,
        'msg' => 'Not Extended',
    ],
    511 => [
        'code' => 511,
        'msg' => 'Network Authentication Required',
    ],

]

?>
