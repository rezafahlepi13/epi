<?php

return [
    'Sysmex' => [
        'XN1K' => [
            'Sparator' => [
                'Field' => '|',
                'Record' => chr(3),
            ],
            'Parameter' => [
                'DataFieldCount' => 13,
                'HeadFieldCount' => 26,
            ],
            'Definition' => [
                'Count' => [
                    13 => [
                        'Fields' => [
                            null,
                            'rid',
                            'parameter',
                            'value',
                            'unit',
                            null,
                            'status',
                            null,
                            'sex',
                            null,
                            null,
                            null,
                            'datetime',
                        ],
                    ],
                    26 => [
                    ],
                ],
            ],
        ],
        'CA500' => [
            'Sparator' => [
                'Field' => '|',
                'Record' => chr(3),
            ],
            'Parameter' => [
                'DataFieldCount' => 13,
                'HeadFieldCount' => 12,
            ],
            'Definition' => [
                'Count' => [
                    13 => [
                        'Fields' => [
                            null,
                            'rid',
                            'parameter',
                            'value',
                            'unit',
                            null,
                            'status',
                            null,
                            null,
                            null,
                            null,
                            null,
                            'datetime',
                        ],
                    ],
                    12 => [
                    ],
                ],
            ],
        ],
        'CA600' => [
            'Sparator' => [
                'Field' => '|',
                'Record' => chr(3),
            ],
            'Parameter' => [
                'DataFieldCount' => 13,
                'HeadFieldCount' => 12,
            ],
            'Definition' => [
                'Count' => [
                    13 => [
                        'Fields' => [
                            null,
                            'rid',
                            'parameter',
                            'value',
                            'unit',
                            null,
                            'status',
                            null,
                            'sex',
                            null,
                            null,
                            null,
                            'datetime',
                        ],
                    ],
                    12 => [
                    ],
                ],
            ],
        ],
        // masi error
       'CS2100i' => [
            'Sparator' => [
                'Field' => '|',
                'Record' => chr(3),
            ],
            'Parameter' => [
                'DataFieldCount' => 13,
                'HeadFieldCount' => 12,
            ],
            'Definition' => [
                'Count' => [
                    13 => [
                        'Fields' => [
                            null,
                            'rid',
                            'parameter',
                            'value',
                            'unit',
                            null,
                            'status',
                            null,
                            'sex',
                            null,
                            null,
                            null,
                            'datetime',
                        ],
                    ],
                    12 => [
                    ],
                ],
            ],
        ],
        //masi error
        'bm6010' => [
            'Sparator' => [
                'Field' => 'chr(64)',
                'Record' => chr(3),
            ],
            'Parameter' => [
                'DataFieldCount' => 77,
                'HeadFieldCount' => 77,
            ],
            'Definition' => [
                'Count' => [
                    77 => [
                        'Fields' => [
                            null,
                            'rid',
                            'parameter',
                            'value',
                            'unit',
                            null,
                            'status',
                            null,
                            'sex',
                            null,
                            null,
                            'datetime',
                        ],
                    ],
                    77 => [
                    ],
                ],
            ],
        ],
    ],
    'Uro_Meter720'=>[
      '720' => [
            'Sparator' => [
                'Field' => " ",
                'Record' => chr(3),
            ],
            'Parameter' => [
                'DataFieldCount' => 3,
                'HeadFieldCount' => 3,
            ],
            'Definition' => [
                'Count' => [
                    14 => [
                        'Fields' => [
                            'parameter',
                            'status',

                        ],
                    ],
                    26 => [
                    ],
                ],
            ],
        ],  
    ]

    //eror di []
    'Nova' => [
        'Phox' => [
            'Sparator' => [
                'Field' => '|',
                'Record' => chr(3),
            ],
            'Parameter' => [
                'DataFieldCount' => 14,
                'HeadFieldCount' => 26,
            ],
            'Definition' => [
                'Count' => [
                    14 => [
                        'Fields' => [
                            null,
                            'rid',
                            'parameter',
                            'value',
                            'unit',
                            null,
                            'status',
                            null,
                            'sex',
                            null,
                            null,
                            'datetime',
                            null,
                            null,
                        ],
                    ],
                    26 => [
                    ],
                ],
            ],
        ],
        'CRT-5'=>[
            'Sparator'=>[
                 'Field' => '\\' ,
                 'Record' => chr(3),
        ],
       'Parameter' => [
                'DataFieldCount' => 10,
                'HeadFieldCount' => 1,
        ],
        'Definition' => [
                'Count' => [
                    10 => [
                        'Fields' => [
                            'rid',
                            'parameter',
                            'value',
                            'unit',
                            null,
                            null,
                            null,
                            null,
                            null,
                            'date',
                        ],
                    ],
                    11=> [
                    ],
                ],
            ],
        ],
    ],

];
