<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'segment' => 1,
                'category' => 'Chemistry',
                'created_at' => '2018-02-28 12:44:01',
                'updated_at' => '2018-02-28 12:44:01',
            ),
            1 => 
            array (
                'id' => 2,
                'segment' => 1,
                'category' => 'Coagulation',
                'created_at' => '2018-02-28 12:44:01',
                'updated_at' => '2018-02-28 12:44:01',
            ),
            2 => 
            array (
                'id' => 3,
                'segment' => 1,
                'category' => 'Electrolyte',
                'created_at' => '2018-02-28 12:44:01',
                'updated_at' => '2018-02-28 12:44:01',
            ),
            3 => 
            array (
                'id' => 4,
                'segment' => 1,
                'category' => 'Hematology',
                'created_at' => '2018-02-28 12:44:01',
                'updated_at' => '2018-02-28 12:44:01',
            ),
            4 => 
            array (
                'id' => 5,
                'segment' => 1,
                'category' => 'Immunology',
                'created_at' => '2018-02-28 12:44:01',
                'updated_at' => '2018-02-28 12:44:01',
            ),
        ));
        
        
    }
}