<?php

use Illuminate\Database\Seeder;

class InstrumentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('Instruments')->delete();
        
        \DB::table('Instruments')->insert(array (
            0 => 
            array (
                'id' => 1,
                'vendor' => 1,
                'instrument' => 'Architect C4000',
                'created_at' => '2018-02-28 12:44:01',
                'updated_at' => '2018-02-28 12:44:01',
            ),
            1 => 
            array (
                'id' => 2,
                'vendor' => 1,
                'instrument' => 'Architect C4000-B',
                'created_at' => '2018-02-28 12:44:01',
                'updated_at' => '2018-02-28 12:44:01',
            ),
            2 => 
            array (
                'id' => 3,
                'vendor' => 10,
                'instrument' => 'Ave 764-752',
                'created_at' => '2018-02-28 12:44:01',
                'updated_at' => '2018-02-28 12:44:01',
            ),
            3 => 
            array (
                'id' => 4,
                'vendor' => 3,
                'instrument' => 'Biolyte 2000',
                'created_at' => '2018-02-28 12:44:01',
                'updated_at' => '2018-02-28 12:44:01',
            ),
            4 => 
            array (
                'id' => 5,
                'vendor' => 9,
                'instrument' => 'BM 6010',
                'created_at' => '2018-02-28 12:44:01',
                'updated_at' => '2018-02-28 12:44:01',
            ),
            5 => 
            array (
                'id' => 6,
                'vendor' => 6,
                'instrument' => 'BT3500-B',
                'created_at' => '2018-02-28 12:44:01',
                'updated_at' => '2018-02-28 12:44:01',
            ),
            6 => 
            array (
                'id' => 7,
                'vendor' => 5,
                'instrument' => 'D10 Lab',
                'created_at' => '2018-02-28 12:44:01',
                'updated_at' => '2018-02-28 12:44:01',
            ),
            7 => 
            array (
                'id' => 8,
                'vendor' => 8,
                'instrument' => 'Dialab Autolyzer',
                'created_at' => '2018-02-28 12:44:01',
                'updated_at' => '2018-02-28 12:44:01',
            ),
            8 => 
            array (
                'id' => 9,
                'vendor' => 7,
                'instrument' => 'Kryptor Compact Plus',
                'created_at' => '2018-02-28 12:44:01',
                'updated_at' => '2018-02-28 12:44:01',
            ),
            9 => 
            array (
                'id' => 10,
                'vendor' => 11,
                'instrument' => 'Nova 5 Elektrolit',
                'created_at' => '2018-02-28 12:44:01',
                'updated_at' => '2018-02-28 12:44:01',
            ),
            10 => 
            array (
                'id' => 11,
                'vendor' => 11,
                'instrument' => 'Nova Phox',
                'created_at' => '2018-02-28 12:44:01',
                'updated_at' => '2018-02-28 12:44:01',
            ),
            11 => 
            array (
                'id' => 12,
                'vendor' => 10,
                'instrument' => 'StatStrip',
                'created_at' => '2018-02-28 12:44:01',
                'updated_at' => '2018-02-28 12:44:01',
            ),
            12 => 
            array (
                'id' => 13,
                'vendor' => 13,
                'instrument' => 'Sysmex C104',
                'created_at' => '2018-02-28 12:44:01',
                'updated_at' => '2018-02-28 12:44:01',
            ),
            13 => 
            array (
                'id' => 14,
                'vendor' => 13,
                'instrument' => 'Sysmex CA 500',
                'created_at' => '2018-02-28 12:44:01',
                'updated_at' => '2018-02-28 12:44:01',
            ),
            14 => 
            array (
                'id' => 15,
                'vendor' => 13,
                'instrument' => 'Sysmex CA 600',
                'created_at' => '2018-02-28 12:44:01',
                'updated_at' => '2018-02-28 12:44:01',
            ),
            15 => 
            array (
                'id' => 16,
                'vendor' => 13,
                'instrument' => 'Sysmex CS2100i',
                'created_at' => '2018-02-28 12:44:01',
                'updated_at' => '2018-02-28 12:44:01',
            ),
            16 => 
            array (
                'id' => 17,
                'vendor' => 13,
                'instrument' => 'Sysmex XN 1000',
                'created_at' => '2018-02-28 12:44:01',
                'updated_at' => '2018-02-28 12:44:01',
            ),
            17 => 
            array (
                'id' => 18,
                'vendor' => 2,
                'instrument' => 'Triage MeterPro',
                'created_at' => '2018-02-28 12:44:01',
                'updated_at' => '2018-02-28 12:44:01',
            ),
            18 => 
            array (
                'id' => 19,
                'vendor' => 12,
                'instrument' => 'Urometer 720',
                'created_at' => '2018-02-28 12:44:01',
                'updated_at' => '2018-02-28 12:44:01',
            ),
            19 => 
            array (
                'id' => 20,
                'vendor' => 4,
                'instrument' => 'Vidas PC',
                'created_at' => '2018-02-28 12:44:01',
                'updated_at' => '2018-02-28 12:44:01',
            ),
        ));
        
        
    }
}