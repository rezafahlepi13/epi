<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(VendorsTableSeeder::class);
        $this->call(SegmentsTableSeeder::class);
        $this->call(InstrumentsTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(InstrumentCategoriesTableSeeder::class);
    }
}
