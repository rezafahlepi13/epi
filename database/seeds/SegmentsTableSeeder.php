<?php

use Illuminate\Database\Seeder;

class SegmentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('Segments')->delete();
        
        \DB::table('Segments')->insert(array (
            0 => 
            array (
                'id' => 1,
                'segment' => 'Instruments',
                'created_at' => '2018-02-28 12:51:17',
                'updated_at' => '2018-02-28 12:51:17',
            ),
        ));
        
        
    }
}