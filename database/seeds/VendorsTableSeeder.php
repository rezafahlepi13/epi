<?php

use Illuminate\Database\Seeder;

class VendorsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('Vendors')->delete();
        
        \DB::table('Vendors')->insert(array (
            0 => 
            array (
                'id' => 1,
                'vendor' => 'Abbott Core',
                'created_at' => '2018-02-28 12:44:01',
                'updated_at' => '2018-02-28 12:44:01',
            ),
            1 => 
            array (
                'id' => 2,
                'vendor' => 'Alere',
                'created_at' => '2018-02-28 12:44:12',
                'updated_at' => '2018-02-28 12:44:12',
            ),
            2 => 
            array (
                'id' => 3,
                'vendor' => 'BioCare',
                'created_at' => '2018-02-28 12:44:19',
                'updated_at' => '2018-02-28 12:44:19',
            ),
            3 => 
            array (
                'id' => 4,
                'vendor' => 'Biomerux',
                'created_at' => '2018-02-28 12:44:26',
                'updated_at' => '2018-02-28 12:44:26',
            ),
            4 => 
            array (
                'id' => 5,
                'vendor' => 'Bio-Rad',
                'created_at' => '2018-02-28 12:44:35',
                'updated_at' => '2018-02-28 12:44:35',
            ),
            5 => 
            array (
                'id' => 6,
                'vendor' => 'Biotecnica',
                'created_at' => '2018-02-28 12:44:46',
                'updated_at' => '2018-02-28 12:44:46',
            ),
            6 => 
            array (
                'id' => 7,
                'vendor' => 'Brahms Instruments',
                'created_at' => '2018-02-28 12:44:55',
                'updated_at' => '2018-02-28 12:44:55',
            ),
            7 => 
            array (
                'id' => 8,
                'vendor' => 'Dialab',
                'created_at' => '2018-02-28 12:45:02',
                'updated_at' => '2018-02-28 12:45:02',
            ),
            8 => 
            array (
                'id' => 9,
                'vendor' => 'Diasys',
                'created_at' => '2018-02-28 12:45:08',
                'updated_at' => '2018-02-28 12:45:08',
            ),
            9 => 
            array (
                'id' => 10,
                'vendor' => 'Keyin',
                'created_at' => '2018-02-28 12:45:13',
                'updated_at' => '2018-02-28 12:45:13',
            ),
            10 => 
            array (
                'id' => 11,
                'vendor' => 'Nova Biomedical Corp.',
                'created_at' => '2018-02-28 12:45:23',
                'updated_at' => '2018-02-28 12:45:23',
            ),
            11 => 
            array (
                'id' => 12,
                'vendor' => 'Standar Diagnostic Inc.',
                'created_at' => '2018-02-28 12:45:31',
                'updated_at' => '2018-02-28 12:45:31',
            ),
            12 => 
            array (
                'id' => 13,
                'vendor' => 'Sysmex Corporation',
                'created_at' => '2018-02-28 12:45:42',
                'updated_at' => '2018-02-28 12:45:42',
            ),
        ));
        
        
    }
}