<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstrumentLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instrument_logs', function (Blueprint $table) {
            $table->increments('id')
                ->unsigned()
            ;

            $table->integer('instrument')
                ->unsigned()
            ;

            $table->foreign('instrument')
                ->references('id')
                ->on('instruments')
            ;

            $table->integer('log')
                ->unsigned()
            ;

            $table->foreign('log')
                ->references('id')
                ->on('logs')
            ;

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instrument_logs');
    }
}
