<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstrumentRawsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instrument_raws', function (Blueprint $table) {
            $table->increments('id')
                ->unsigned();

            $table->integer('instrument')
                ->unsigned()
            ;
            $table->foreign('instrument')
                ->references('id')
                ->on('instruments')
            ;

            $table->string('uid');

            $table->integer('rid')
                ->unsigned()
            ;

            $table->string('data');

            $table->unique('uid', 'rid');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instrument_raws');
    }
}
