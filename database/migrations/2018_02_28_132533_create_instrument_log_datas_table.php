<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstrumentLogDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instrument_log_datas', function (Blueprint $table) {
            $table->increments('id')
                ->unsigned()
            ;

            $table->integer('instrument_log')
                ->unsigned()
            ;

            $table->foreign('instrument_log')
                ->references('id')
                ->on('instrument_logs')
            ;

            $table->integer('rid')
                ->unsigned()
            ;

            $table->string('val');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instrument_log_datas');
    }
}
