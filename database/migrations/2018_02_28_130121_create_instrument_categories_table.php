<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstrumentCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instrument_categories', function (Blueprint $table) {
            $table->increments('id')
                ->unsigned()
            ;

            $table->integer('category')
                ->unsigned()
            ;

            $table->foreign('category')
                ->references('id')
                ->on('categories')
            ;

            $table->integer('instrument')
                ->unsigned()
            ;
            $table->foreign('instrument')
                ->references('id')
                ->on('instruments')
            ;

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instrument_categories');
    }
}
