<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstrumentProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instrument_profiles', function (Blueprint $table) {
            $table->increments('id')
                ->unsigned()
            ;

            $table->integer('instrument')
                ->unsigned()
            ;

            $table->foreign('instrument')
                ->references('id')
                ->on('instruments')
            ;

            $table->string('alias');

            $table->unique('instrument', 'alias');

            $table->string('ip');
            $table->string('host');
            $table->string('location');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instrument_profiles');
    }
}
