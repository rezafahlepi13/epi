# Windmel #

Universal Laboratory Instrument Protocol Translation for your better Life ;).

----------

# How it works ?
Windmel using own service called Windmel-ULIC  for handling data transmission with IPU.
Data will be proceed to own database and managed to friendly REST API.

# Supported Instruments
* Cell Counter
- ☑ Sysmex XN 1000
- ☐ Sysmex KX 21
* Bio-Chemistry
- ☐ Nova CRT 5 Electrolyte
- ☐ Dialab Autolyser
- ☐ Biotecnica BT3500
- ☐ Elitech Selectra Junior 
- ☐ Elitech Selectra E 
- ☐ Biotecnica BT3500
- ☐ Sysmex / Jeol JCA-BM6010/c
- ☐ Abbott  Architect c4000
* Coagulation
- ☐ Sysmex CA 600
* Blood Gas
- ☐ Nova Phox
* Microbiology
- ☐ Biomerieux VITEK® 2 COMPACT
* Urine Analyzer
- ☐ Uro-Meter 720
* Other 
- ☐ D10 (HbA1c)
- ☐ AVE 764-752